import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.sass']
})
export class UserInfoComponent implements OnInit {

  @Input() userInfo: string;
  @Output() logout: EventEmitter<any> = new EventEmitter();
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  navProfile() {
    this.router.navigate(['/profile']);
  }

}
