import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({name: 'filter'})
export class FilterReportPipe implements PipeTransform {

  constructor() { }

  transform( items: any[], query: string ): any {
    if ( !query || query === '' || query.length < 2 ) return items;
    return items.filter( item => {
      const q = this.getSimpleStr( query );
      const anomalyFormState = item.anomalyFormState && this.getSimpleStr( item.anomalyFormState ).indexOf( q ) > -1;
      const anomalyFormId = item.anomalyFormId && this.getSimpleStr( item.anomalyFormId ).indexOf( q ) > -1;
      const userName = item.userName && this.getSimpleStr( item.userName ).indexOf( q ) > -1;
      const userPin = item.userPin && this.getSimpleStr( item.userPin ).indexOf( q ) > -1;
      const userOrganizationManglarName = item.userOrganizationManglarName && this.getSimpleStr( item.userOrganizationManglarName ).indexOf( q ) > -1;
      return anomalyFormState || anomalyFormId || userName || userPin || userOrganizationManglarName;
    } );
  }

  private getSimpleStr( str ) {
    if ( !str || str === '' ) return '';
    let simple1 = str.toString().replace( /[\u0300-\u036f]/g, '' ).replace( / /g, '' );
    return this.noAccents( simple1 ).toLowerCase();
  }

  private noAccents( accentsString ) {
    let r = accentsString.toLowerCase();
    const non_asciis = {'a': '[àáâãäå]', 'ae': 'æ', 'c': 'ç', 'e': '[èéêë]', 'i': '[ìíîï]', 'n': 'ñ', 'o': '[òóôõö]', 'oe': 'œ', 'u': '[ùúûűü]', 'y': '[ýÿ]'};
    Object.keys( non_asciis ).forEach( i => r = r.replace( new RegExp( non_asciis[i], 'g' ), i ) );
    return r;
  };

}
