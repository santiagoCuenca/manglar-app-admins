import { Component, ViewChild, OnInit } from '@angular/core';
import { CaptchaComponent } from 'angular-captcha';
import { Router } from '@angular/router';

import { Api } from '../services/api';
import { Utility } from '../util/utility';
import { Constants } from '../services/constants';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.sass']
})
export class PasswordComponent implements OnInit {

  @ViewChild(CaptchaComponent) captchaComponent: CaptchaComponent;
  user: string = '';
  successSend: boolean = false;

  constructor(
    private api: Api,
    private utility: Utility,
    private router: Router,
  ) { }

  ngOnInit() {
    this.captchaComponent.captchaEndpoint = Constants.HOST_URL + 'simple-captcha-endpoint';
  }


  submit(){
    this.validateCaptcha( () => {
      this.utility.showLoading();
      const keyUrl = btoa(this.utility.getTimestamp().toString());
      const data = {
        userPin: this.user,
        url: Constants.HOST_URI + '#/reset-password/' + keyUrl,
      };
      this.api.recoverPasswordTemp(data).subscribe( result => {
        this.utility.hideLoading();
        if ( result.state === 'OK' ) {
          this.successSend = true;
          // this.utility.showSnackBar( 'Se envió un correo a ' + result.userEmail );
          // this.router.navigate(['/login']);
          return;
        }
        this.utility.showSnackBar( result.state );
      });
    });
  }

  cancel(){
    this.router.navigate(['/login']);
  }

  validateCaptcha(success): void {
    if (!this.user || this.user === ''){
      this.utility.showSnackBar('Debe ingresar su usuario');
      return;
    }
    let userEnteredCaptchaCode = this.captchaComponent.userEnteredCaptchaCode;
    if (!userEnteredCaptchaCode || userEnteredCaptchaCode === ''){
      this.utility.showSnackBar('Debe ingresar el texto que se muestra en la imagen');
      return;
    }
    let captchaId = this.captchaComponent.captchaId;
    const postData = {
      userEnteredCaptchaCode: userEnteredCaptchaCode,
      captchaId: captchaId
    };
    // post the captcha data to the /your-app-backend-path on your backend
    this.api.sendCaptcha(postData)
      .subscribe(
        response => {
          if (response.state === 'OK') {
            success();
          } else {
            this.captchaComponent.reloadImage();
            this.utility.showSnackBar('El texto que ingresó no corresponde a la imagen');
          }
        },
        error => {
          throw new Error(error);
        });
  }

}
