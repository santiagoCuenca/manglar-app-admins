import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  isValidate = false;

  // [{"id":1,"value":"Ecuatoriana"}]
  nationalities: any[] = [];
  // [{"id":17,"value":"ARENILLAS"},{"id":18,"value":"CARCABON"},{"id":19,"value":"CHACRAS"},{"id":20,"value":"PALMALES"}]
  provinces: any[] = [];
  cantons: any[] = [];
  parroquias: any[] = [];

  // USER INFO
  pin: string;
  name: string;
  phone: string;
  mobile: string;
  email: string;
  address: string;
  organization: string;
  organizationManglarId: number;
  acceptConditions = true;

  // SELECTS
  selectedNationality: any;
  selectedParroquia: any;

  isLoading: boolean = false;

  constructor(
    private api: Api,
    private utility: Utility,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  validatePin(){
    this.isLoading = true;
    this.api.validatePin(this.pin).subscribe( dataValidate => {
      if ( dataValidate.state !== 'OK' ) {
        this.utility.showSnackBar( dataValidate.state );
        this.isLoading = false;
        return;
      }
      this.pin = dataValidate.pin;
      this.name = dataValidate.name;
      this.organization = dataValidate.organizationManglarName;
      this.organizationManglarId = dataValidate.organizationManglarId;

      // fill api values
      this.api.getNationalities().subscribe( nationalities => {
        this.nationalities = nationalities;
        this.isValidate = true;
        this.isLoading = false;
      });
      this.api.getLocations(1).subscribe( provinces => {
        this.provinces = provinces;
      });
    });
  }

  changeCantons(selectProvince){
    this.api.getLocations(selectProvince.id).subscribe( cantons => {
      this.cantons = cantons;
    });
  }

  changeParroquias(selectedCanton){
    this.api.getLocations(selectedCanton.id).subscribe( parroquias => {
      this.parroquias = parroquias;
    });
  }

  register(){
    if ( !this.validateInfo() ) return;
    this.utility.confirmDialog('Confirmar correo', 'Se enviará un correo a ' + this.email + ' ¿Es correcto su correo? De lo contrario, no podrá ingresar', () => {
      this.isLoading = true;
      const dataSave = {
        pin: this.pin,
        name: this.name,
        treatmentId: 1, // TODO EVALUATE IF NECESSARY
        nationalityId: this.selectedNationality.id,
        phone: this.phone,
        mobile: this.mobile,
        email: this.email,
        address: this.address,
        gender: "HOMBRE", // TODO EVALUATE IF NECESSARY
        civilStatus: "SOLTERO", // TODO EVALUATE IF NECESSARY
        parroquiaId: this.selectedParroquia.id,
        organizationManglarId: this.organizationManglarId,
      };
      this.api.saveUser(dataSave).subscribe( result => {
        this.isLoading = false;
        if (result.state === 'OK') {
          this.router.navigate(['/login']);
          this.utility.showSnackBar( 'Se envió un correo con su usuario y clave' );
        } else {
          this.utility.showSnackBar( result.state );
        }
      });
    });
  }

  cancel(){
    this.router.navigate(['/login']);
  }

  validateInfo() {
    if ( !this.selectedNationality ) {
      this.utility.showSnackBar( 'Debe seleccionar su nacionalidad' );
      return false;
    }
    if ( !this.selectedParroquia ) {
      this.utility.showSnackBar( 'Debe seleccionar su ubicación' );
      return false;
    }
    if ( !this.acceptConditions ) {
      this.utility.showSnackBar( 'Debe aceptar las condiciones de uso' );
      return false;
    }
    if ( !this.phone || !this.mobile || !this.address || !this.email ) {
      this.utility.showSnackBar( 'Debe llenar todos los campos' );
      return false;
    }
    if ( this.email.indexOf('@') < 0 ) {
      this.utility.showSnackBar( 'Debe ingresar un correo valido' );
      return false;
    }
    if ( this.mobile.length != 10 ) {
      this.utility.showSnackBar( 'Debe ingresar un celular valido' );
      return false;
    }
    if ( this.phone.length < 6 ) {
      this.utility.showSnackBar( 'Debe ingresar un número fijo valido' );
      return false;
    }
    return true;
  }

}
