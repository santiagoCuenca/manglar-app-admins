import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-block-info',
  templateUrl: './block-info.component.html',
  styleUrls: ['./block-info.component.sass']
})
export class BlockInfoComponent implements OnInit {

  @Input() title;
  @Input() width: number;
  @Input('title-center') titleCenter: boolean;

  constructor() { }

  ngOnInit() {
    this.titleCenter = this.titleCenter !== undefined;
    this.width = this.width || 95;
  }

}
