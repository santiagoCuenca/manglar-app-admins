import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-power-bi-extra-info',
  templateUrl: './power-bi-extra-info.component.html',
  styleUrls: ['./power-bi-extra-info.component.sass']
})
export class PowerBiExtraInfoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onClick(){
    const url = 'https://app.powerbi.com/reportEmbed?reportId=c8e1af7a-15b4-48c1-bc8a-23767ed75dcb&autoAuth=true&ctid=d7c45676-6c73-4b4c-91ef-f5a72a90abf7&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXBhYXMtMS1zY3VzLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0LyJ9';
    window.open(url,'_blank');
  }

}
