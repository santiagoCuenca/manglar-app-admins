import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerBiExtraInfoComponent } from './power-bi-extra-info.component';

describe('PowerBiExtraInfoComponent', () => {
  let component: PowerBiExtraInfoComponent;
  let fixture: ComponentFixture<PowerBiExtraInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerBiExtraInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerBiExtraInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
