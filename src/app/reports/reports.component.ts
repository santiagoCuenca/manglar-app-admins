import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Api } from '../services/api';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.sass']
})
export class ReportsComponent implements OnInit, OnChanges {

  @Input() idReport;
  reports: any[] = [];
  refConfig: any; // undefinde by default
  isLoading: boolean = true;
  searchquery: string = '';

  constructor(
    private api: Api,
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.isLoading = true;
    this.update();
  }

  private update(){
    this.api.getReportsByType(this.idReport).subscribe( reportsApi => {
      this.reports = reportsApi;
      this.isLoading = false;
    });
  }

}
