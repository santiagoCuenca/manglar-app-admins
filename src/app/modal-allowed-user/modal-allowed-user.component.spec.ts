import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAllowedUserComponent } from './modal-allowed-user.component';

describe('ModalAllowedUserComponent', () => {
  let component: ModalAllowedUserComponent;
  let fixture: ComponentFixture<ModalAllowedUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAllowedUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAllowedUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
