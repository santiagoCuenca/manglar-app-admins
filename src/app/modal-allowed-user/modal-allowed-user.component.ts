import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

export interface DialogData {
  type: string,
  allowedUserId: number;
  allowedUserName: string;
  allowedUserPin: string;
  allowedUserStatus: boolean;
  geographicalLocationName: string;
  organizationManglarName: string;
}

@Component({
  selector: 'app-modal-allowed-user',
  templateUrl: './modal-allowed-user.component.html',
  styleUrls: ['./modal-allowed-user.component.sass']
})
export class ModalAllowedUserComponent {

  organizations: any[] = [];
  provinces: any[] = [];
  evaluatedPin: string;

  isSuperAdmin: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<ModalAllowedUserComponent>,
    private api: Api,
    private utility: Utility,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
    this.isSuperAdmin = this.data.type === 'super-admin';
    if (!!this.data.allowedUserPin) {
      this.evaluatedPin = this.data.allowedUserPin;
    }
    this.api.getOrganizations().subscribe(organizations => {
      this.organizations = organizations.filter(
        o => o.organizationManglarStatus && o.organizationManglarType === this.data.type
      );
    });
    this.api.getProvinces().subscribe(provinces => {
      this.provinces = provinces;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(){
    const province = this.provinces.find( p => p.state === this.data.geographicalLocationName );
    const organizationManglar = this.organizations.find( o => o.organizationManglarName === this.data.organizationManglarName );
    if (!this.organizations.length) {
      this.utility.showSnackBar('ERROR: Debe primero crear una organización');
      return;
    }
    if (!organizationManglar && !this.isSuperAdmin) {
      this.utility.showSnackBar('ERROR: No seleccionó una organización valida');
      return;
    }
    if (!province && !this.isSuperAdmin) {
      this.utility.showSnackBar('ERROR: No seleccionó una provincia valida');
      return;
    }
    if (this.data.allowedUserName === '' || this.data.allowedUserPin === '' || this.data.allowedUserPin.length !== 10 ){
      this.utility.showSnackBar('ERROR: Nombre o cédula no valida');
      return;
    }
    const geloId = this.isSuperAdmin ? 1 : province.id; // 1 is Ecuador
    const organizationManglarId = this.isSuperAdmin ?
      this.organizations[0].organizationManglarId :
      organizationManglar.organizationManglarId;

    const dataSubmit = {
      allowedUser: {
        allowedUserStatus: this.data.allowedUserStatus,
        allowedUserName: this.data.allowedUserName,
        allowedUserPin: this.data.allowedUserPin,
      },
      geloId,
      organizationManglarId,
    };
    if (this.data.allowedUserId) {
      dataSubmit.allowedUser['allowedUserId'] = this.data.allowedUserId;
    }
    this.dialogRef.close(dataSubmit);
  }

  updateNameFromCedula(){
    if (!this.data.allowedUserPin || this.data.allowedUserPin === '' || this.data.allowedUserPin.length != 10){
      this.utility.showSnackBar('ERROR: Cédula no valida');
      return;
    }
    this.api.getCedulaInfo(this.data.allowedUserPin).subscribe( cedulaInfo => {
      if (cedulaInfo.error === "NO ERROR"){
        this.evaluatedPin = this.data.allowedUserPin;
        this.data.allowedUserName = cedulaInfo.nombre;
      }
      else{
        this.data.allowedUserName = '';
        this.utility.showSnackBar('ERROR: Cédula no valida');
      }
    });
  }

  wasEvaluated(){
    return this.evaluatedPin === this.data.allowedUserPin;
  }

}
