import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  username: string = '';
  password: string = '';

  constructor(
    private router: Router,
    private api: Api,
    private utility: Utility,
  ) {

  }

  ngOnInit() {
    if ( !this.utility.isNotLogged() ) {
      this.router.navigate(['/dashboard']);
    }
  }

  public login(){
    if ( this.username === '' || this.password === '') {
      this.utility.showSnackBar( 'Debe ingresar usuario y contraseña' );
      return;
    }
    const data = {
      username: this.username,
      password: this.password,
    };
    this.api.accessUser(data).subscribe( result => {
      if ( result.state === 'OK' ) {
        this.utility.showSnackBar( 'Bienvenido ' + result.userName );
        this.utility.login(result.userName, result.userId, result.isSuperAdmin, result.userPin, this.password);
        this.router.navigate(['/dashboard']);
      }
      else {
        this.utility.showSnackBar( result.state );
      }
    });

  }

  public register(){
    this.router.navigate(['/register']);
  }

  public recoverPassword() {
    this.router.navigate(['/reset-password']);
  }

}
