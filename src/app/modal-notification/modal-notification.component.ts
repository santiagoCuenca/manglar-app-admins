import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Api } from '../services/api';
import { Utility } from '../util/utility';
import { Constants } from '../services/constants';

export interface DialogData {
  emailNotificationId: number;
  emailNotificationAnomaliesTypes: string;
  emailNotificationEmail: string;
  emailNotificationName: string;
  emailNotificationProvinces: string;
  emailNotificationStatus: boolean;
}

@Component({
  selector: 'app-modal-notification',
  templateUrl: './modal-notification.component.html',
  styleUrls: ['./modal-notification.component.sass']
})
export class ModalNotificationComponent {

    provinces: any[] = [];

    reports: any[] = [];

    provincesSelected = [];
    typesSelected = [];

    constructor(
      public dialogRef: MatDialogRef<ModalNotificationComponent>,
      private api: Api,
      private utility: Utility,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {

      this.reports = Object.keys(Constants.REPORTS).map( key => Constants.REPORTS[key] );
      this.formatAll();

      this.api.getProvinces().subscribe(provinces => {
        this.provinces = provinces;
        this.formatAll();
      });
    }

    private formatAll(){
      if ( this.data.emailNotificationAnomaliesTypes === 'ALL' ) {
        this.typesSelected = this.reports.map(r => r.title);
      }else{
        this.typesSelected = JSON.parse(this.data.emailNotificationAnomaliesTypes)
          .map(id => this.reports.find(r => r.idReport === id).title);
      }
      if ( this.data.emailNotificationProvinces === 'ALL' ) {
        this.provincesSelected = this.provinces.map( p => p.state);
      }else{
        this.provincesSelected = JSON.parse(this.data.emailNotificationProvinces);
      }
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    onSubmit(){
      if (!this.provincesSelected.length){
        this.utility.showSnackBar('ERROR: Debe seleccionar una provincia');
        return;
      }
      if (!this.typesSelected.length){
        this.utility.showSnackBar('ERROR: Debe seleccionar una tipo de alerta');
        return;
      }
      if (this.data.emailNotificationName === '' || this.data.emailNotificationEmail === ''){
        this.utility.showSnackBar('ERROR: Debe ingresar nombre y correo');
        return;
      }
      const emailNotificationProvinces = this.provincesSelected.length === this.provinces.length
        ? 'ALL' : JSON.stringify(this.provincesSelected);
      const typesToIds = this.typesSelected.map( provinceName => {
        return this.reports.find( p => p.title === provinceName).idReport;
      });
      const emailNotificationAnomaliesTypes = typesToIds.length === this.reports.length
        ? 'ALL' : JSON.stringify(typesToIds);
      const update = {
        emailNotificationProvinces,
        emailNotificationAnomaliesTypes,
      };
      const dataSubmit = Object.assign(this.data, update);
      this.dialogRef.close(dataSubmit);
    }

}
