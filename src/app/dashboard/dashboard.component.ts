import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { environments } from '../../environments/environments';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  param1: string = 'no_selected'; // By default
  param2: string = 'no_selected'; // By default
  param3: string = 'no_selected'; // By default
  version: string = environments.version;
  username: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utility: Utility,
  ) { }

  ngOnInit() {
    if ( this.utility.isNotLogged() ) {
      this.logout();
      setTimeout( () => this.utility.showSnackBar('Usuario no inició sesión'));
      return;
    }
    // IDLE AUTO CLOSE SESSION
    this.utility.startIdle();

    this.route.params.subscribe(params => {
        this.param1 = params['param1'];
        this.param2 = params['param2'];
        this.param3 = params['param3'];
    });
    this.username = this.utility.getLocalStorage('username');
  }

  logout(){
    this.utility.logout();
    this.router.navigate(['/login']);
  }
}
