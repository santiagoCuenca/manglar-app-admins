import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Api } from '../services/api';
import { Utility } from '../util/utility';
import { Constants } from '../services/constants';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.sass']
})
export class PasswordResetComponent implements OnInit {

  user: string = '';
  newPassword: string = '';
  confirmPassword: string = '';
  param1: string = 'no_selected'; // By default
  isValid: boolean = false;

  constructor(
    private api: Api,
    private utility: Utility,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.param1 = params['param1'];
      this.isValid = this.isValidT(this.param1);
    });
  }

  isValidT(tsParam){
    const tsUrl = parseInt(atob(tsParam));
    console.log(tsUrl);
    if (Number.isNaN(tsUrl)) return false;
    const tsValid = 15 * 60 * 1000; // 15min
    const ts = this.utility.getTimestamp();
    return (ts >= tsUrl && ts <= (tsUrl + tsValid));
  }

  submit(){
    if (this.user === '' || this.newPassword === '' || this.confirmPassword === ''){
      this.utility.showSnackBar('Debe llenar todos los campos');
      return;
    }
    if (this.newPassword !== this.confirmPassword) {
      this.utility.showSnackBar('La nueva contraseña y la confirmación no coinciden');
      return;
    }
    this.utility.showLoading();
    const data = {
      userPin: this.user,
      password: null, // IS NOT NEEDED
      newPassword: this.newPassword,
      newPasswordConfirm: this.confirmPassword,
    };
    this.api.changePasswordNoOld(data).subscribe( result => {
      this.utility.hideLoading();
      if (result.state === 'OK') {
        this.utility.showSnackBar('Se actualizó su contraseña con exito');
        this.router.navigate(['/login']);
        return;
      }
      this.utility.showSnackBar(result.state);
    });
  }

  cancel(){
    this.router.navigate(['/login']);
  }

}
