import { Component, Input, OnInit } from '@angular/core';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.sass']
})
export class TabsComponent implements OnInit {

  @Input() selected;
  @Input() subSelected;

  isSuperAdmin: boolean = false;

  constructor(
    private api: Api,
    private utility: Utility,
  ) { }

  ngOnInit() {
    this.isSuperAdmin = this.utility.getLocalStorage('isSuperAdmin') === 'true';
  }

  style(idTab){
    if ( this.selected !== idTab && this.subSelected !== idTab ) return {};
    return {
      'font-weight': 'bold',
      color: '#4c990c'
    };
  }

}
