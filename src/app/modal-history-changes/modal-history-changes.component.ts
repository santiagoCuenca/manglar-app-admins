import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface History {
  date: number;
  userName: string;
  oldState: string;
  newState: string;
}

export interface DialogData {
  title: string;
  histories: History[];
}

@Component({
  selector: 'app-modal-history-changes',
  templateUrl: './modal-history-changes.component.html',
  styleUrls: ['./modal-history-changes.component.sass']
})
export class ModalHistoryChangesComponent {

  constructor(
    public dialogRef: MatDialogRef<ModalHistoryChangesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
