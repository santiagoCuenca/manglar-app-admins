import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHistoryChangesComponent } from './modal-history-changes.component';

describe('ModalHistoryChangesComponent', () => {
  let component: ModalHistoryChangesComponent;
  let fixture: ComponentFixture<ModalHistoryChangesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalHistoryChangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHistoryChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
