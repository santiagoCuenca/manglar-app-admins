import { Injectable } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Constants } from '../services/constants';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { UserIdleService } from 'angular-user-idle';

@Injectable()
export class Utility {

  idleStarted: boolean = false;
  idleSubscribed: boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService,
    private userIdle: UserIdleService,
  ) {
  }

  getFormatReportsSummary(reportsApi){
    const reportsSummaryObj = reportsApi
      .reduce( (obj, r) => {
        obj[r.id] = r.total;
        return obj;
      }, {});

    return Object.keys(Constants.REPORTS).map( id => {
      return {
        id,
        name: Constants.REPORTS[id].title.toLowerCase(),
        nReports: reportsSummaryObj[id] || 0,
        imgSrc: 'assets/svg/' + id + '.svg',
      }
    })
    .sort( (a, b) => b.nReports - a.nReports );
  }

  showSnackBar(message, timestamp = 5000){
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: timestamp,
      data: message || 'no_message',
    });
  }

  setLocalStorage(key: string, value){
    localStorage.setItem(key, value);
  }

  getLocalStorage(key: string){
    return localStorage.getItem(key);
  }

  removeLocalStorage(key: string){
    return localStorage.removeItem(key);
  }

  login(username, userid, isSuperAdmin, userpin, pass){
    this.setLocalStorage('username', username);
    this.setLocalStorage('userpin', userpin);
    this.setLocalStorage('pass', pass);
    this.setLocalStorage('userid', userid);
    this.setLocalStorage('isSuperAdmin', isSuperAdmin);
  }

  logout(){
    this.removeLocalStorage('username');
    this.removeLocalStorage('userpin');
    this.removeLocalStorage('pass');
    this.removeLocalStorage('userid');
    this.removeLocalStorage('isSuperAdmin');
  }

  isNotLogged(){
    return this.getLocalStorage('username') == null ||
      this.getLocalStorage('userid') == null ||
      this.getLocalStorage('userpin') == null ||
      this.getLocalStorage('pass') == null;
  }

  confirmDialog(title, message, success): void {

    const dialogData = new ConfirmDialogModel(title, message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      width: "300px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        success();
      }
    });
  }

  limitString(value, limit){
    const trail = '...';
    return value.length > limit ? value.substring(0, limit) + trail : value;
  }

  showLoading(){
    this.spinnerService.show();
  }

  hideLoading(){
    this.spinnerService.hide();
  }

  startIdle(){
    if (this.idleStarted) return;
    this.idleStarted = true;
    this.userIdle.startWatching();

    if (this.idleSubscribed) return;
    this.idleSubscribed = true;
    this.userIdle.onTimerStart().subscribe(count => {});
    this.userIdle.onTimeout().subscribe(() => {
      this.userIdle.resetTimer();
      this.userIdle.stopWatching();
      this.idleStarted = false;
      this.logout();
      this.router.navigate(['/login']);
    });
  }

  getTimestamp(){
    return new Date().getTime();
  }

  formatDate(date) {
    var month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('/');
  }

}
