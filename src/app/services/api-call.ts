import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Constants } from './constants';

import { Utility } from '../util/utility';

@Injectable()
export class ApiCall {

  constructor(
    private http: HttpClient,
    private utility: Utility,
  ) {
  }

  get(url){
    return this.http.get<any>(url, this.getHttpOptions());
  }

  post(url, data){
    return this.http.post<any>(url, data, this.getHttpOptions());
  }

  getList(url){
    return this.http.get<any[]>(url, this.getHttpOptions());
  }

  getHttpOptions(){
    const pin = this.utility.getLocalStorage('userpin');
    const pass = this.utility.getLocalStorage('pass');
    if ( pin != null && pass != null ){
      return {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Basic ' + btoa(pin + ':' + pass),
          'ServerUrl': Constants.HOST_URL,
        })
      };
    }
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
  }

}
