export class Constants {

   //public static HOST_URL = 'http://localhost:8080/manglar-app/'; // PRUEBAS
   //public static HOST_URL = 'http://52.204.108.31/manglar-app/'; // PRODUCCION
   public static HOST_URL = 'http://qa-suiaint.ambiente.gob.ec:8099/manglar-app/'; // SUIA TEST

  public static HOST_URI = '/manglar-admin/'; // URI MANGLAR APP WEB

  public static REPORT_STATES = ["Informe técnico", "Inicio proceso administrativo", "Archivo temporal por falta de datos del presunto infractor", "Resolución", "Revisión", "Apelación", "Sanción"];

  public static REPORTS = {
     "contaminacion":{
        "idReport":"contaminacion",
        "title":"CONTAMINACIÓN"
     },
     "epoca_veda":{
        "idReport":"epoca_veda",
        "title":"RECOLECCIÓN EN EPOCA DE VEDA"
     },
     "invasiones":{
        "idReport":"invasiones",
        "title":"INVASIONES"
     },
     "delincuencia_maritima":{
        "idReport":"delincuencia_maritima",
        "title":"DELINCUENCIA MARÍTIMA"
     },
     "tala":{
        "idReport":"tala",
        "title":"TALA"
     },
     "tallas_minimas":{
        "idReport":"tallas_minimas",
        "title":"INCUMPLIMIENTO DE TALLAS MINIMAS"
     },
     "tecnicas_pesca":{
        "idReport":"tecnicas_pesca",
        "title":"TÉCNICAS DE PESCA NO PERMITIDAS"
     },
     "trafico_especies":{
        "idReport":"trafico_especies",
        "title":"TRÁFICO Y PESCA DE ESPECIES AMENAZADAS"
     },
     "vida_silvestre":{
        "idReport":"vida_silvestre",
        "title":"HALLAZGOS DE VIDA SILVESTRE"
     }
  };

}
