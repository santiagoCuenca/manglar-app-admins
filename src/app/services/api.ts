import { Injectable } from '@angular/core';
import { ApiCall } from './api-call';
import { Constants } from './constants';
import { Utility } from '../util/utility';

@Injectable()
export class Api {

  reports;

  constructor(
    private apiCall: ApiCall,
    private utility: Utility,
  ) {
  }

  getReportsSummary(){
    const pin = this.utility.getLocalStorage('userpin');
    const url = Constants.HOST_URL + 'rest/anomaly-form/get-summary-pin/' + pin;
    return this.apiCall.getList(url);
  }

  getReportsByType(type){
    const pin = this.utility.getLocalStorage('userpin');
    const url = Constants.HOST_URL + 'rest/anomaly-form/get-by-type-pin/' + type + '/' + pin;
    return this.apiCall.getList(url);
  }

  getAllReports(){
    const url = Constants.HOST_URL + 'rest/anomaly-form/get-all';
    return this.apiCall.getList(url);
  }

  getReportById(id){
    const url = Constants.HOST_URL + 'rest/anomaly-form/get-by-id/' + id;
    return this.apiCall.get(url);
  }

  getReportHistoyById(id){
    const url = Constants.HOST_URL + 'rest/history-changes/filter/' + id;
    return this.apiCall.get(url);
  }

  getNationalities(){
    const url = Constants.HOST_URL + 'rest/register/nationalities';
    return this.apiCall.getList(url);
  }

  getUserProfile(pin){
    const url = Constants.HOST_URL + 'rest/user-profile/get/' + pin;
    return this.apiCall.get(url);
  }

  saveProfile(data){
    const url = Constants.HOST_URL + 'rest/user-profile/save';
    return this.apiCall.post(url, data);
  }

  getCedulaInfo(cedula){
    const url = Constants.HOST_URL + 'rest/register/pin/' + cedula;
    return this.apiCall.get(url);
  }

  // EMAIL NOTIFICATIONS
  getEmailNotifications(){
    const url = Constants.HOST_URL + 'rest/email-notification/get';
    return this.apiCall.getList(url);
  }

  saveEmailNotification(data){
    const url = Constants.HOST_URL + 'rest/email-notification/save';
    return this.apiCall.post(url, data);
  }

  removeEmailNotification(data){
    const url = Constants.HOST_URL + 'rest/email-notification/remove';
    return this.apiCall.post(url, data);
  }
  // ----------------------

  // ALLOWED USER
  getAllowedUsers(type: string){
    const url = Constants.HOST_URL + 'rest/allowed-user/get-' + type;
    return this.apiCall.getList(url);
  }

  saveAllowedUser(data, type){
    const url = Constants.HOST_URL + 'rest/allowed-user/save-' + type;
    return this.apiCall.post(url, data);
  }

  removeAllowedUser(data){
    const url = Constants.HOST_URL + 'rest/allowed-user/remove';
    return this.apiCall.post(url, data);
  }
  // ----------------------

  // ORGANIZATIONS
  getOrganizations(){
    const url = Constants.HOST_URL + 'rest/organization-manglar/get';
    return this.apiCall.getList(url);
  }

  saveOrganization(data){
    const url = Constants.HOST_URL + 'rest/organization-manglar/save';
    return this.apiCall.post(url, data);
  }

  removeOrganization(data){
    const url = Constants.HOST_URL + 'rest/organization-manglar/remove';
    return this.apiCall.post(url, data);
  }
  // ----------------------

  getProvinces(){
    const url = Constants.HOST_URL + 'rest/register/locations?parentId=1';
    return this.apiCall.getList(url);
  }

  getLocations(parentId){
    const url = Constants.HOST_URL + 'rest/register/locations?parentId=' + parentId;
    return this.apiCall.getList(url);
  }

  validatePin(pin){
    const url = Constants.HOST_URL + 'rest/validate-pin/admin';
    const pinObj = {
      userPin: pin
    };
    return this.apiCall.post(url, pinObj);
  }

  saveUser(data){
    const url = Constants.HOST_URL + 'rest/register/save';
    return this.apiCall.post(url, data);
  }

  accessUser(data){
    const url = Constants.HOST_URL + 'rest/login/access-admin';
    return this.apiCall.post(url, data);
  }

  recoverPassword(data){
    const url = Constants.HOST_URL + 'rest/register/recover-password';
    return this.apiCall.post(url, data);
  }

  recoverPasswordTemp(data){
    const url = Constants.HOST_URL + 'rest/register/recover-password-temp';
    return this.apiCall.post(url, data);
  }

  changePassword(data){
    const url = Constants.HOST_URL + 'rest/register/change-password';
    return this.apiCall.post(url, data);
  }

  changePasswordNoOld(data){
    const url = Constants.HOST_URL + 'rest/register/change-password-no-old';
    return this.apiCall.post(url, data);
  }

  updateState(data){
    const url = Constants.HOST_URL + 'rest/anomaly-form/update-state';
    return this.apiCall.post(url, data);
  }

  sendCaptcha(data){
    const url = Constants.HOST_URL + 'rest/captcha-validate';
    return this.apiCall.post(url, data);
  }

}
