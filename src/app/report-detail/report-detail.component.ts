import { Component, OnInit, Input } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ModalImgComponent } from '../modal-img/modal-img.component';
import { ModalSelectComponent } from '../modal-select/modal-select.component';
import { ModalHistoryChangesComponent } from '../modal-history-changes/modal-history-changes.component';
import { Api } from '../services/api';
import { Utility } from '../util/utility';
import { Constants } from '../services/constants';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.sass']
})
export class ReportDetailComponent implements OnInit {

  @Input() idReport;
  @Input() idReportCreated;
  report: any;

  loading: boolean = true;
  validReport: boolean = true;
  reportStates: string[] = Constants.REPORT_STATES;
  histories: any = [];

  constructor(
    private utility: Utility,
    public dialog: MatDialog,
    private api: Api,
  ) {
  }

  ngOnInit() {
    this.update();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalSelectComponent, {
      width: '360px',
      data: { title: 'Seleccionar nuevo estado:', options: this.reportStates, current: this.report.anomalyFormState }
    });

    dialogRef.afterClosed().subscribe(state => {
      this.updateState(state);
    });
  }

  openDialogHistory(): void {
    const dialogRef = this.dialog.open(ModalHistoryChangesComponent, {
      width: '360px',
      data: { title: 'Historial de cambios:', histories: this.histories }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  updateState(state){
    if (!state) return;
    if (this.report.anomalyFormState === state) {
      this.utility.showSnackBar('No se realizó cambios');
      return;
    }
    this.loading = true;
    const data = {
      id: this.report.anomalyFormId,
      state: state,
      idUserChange: this.utility.getLocalStorage('userid'),
    };
    this.api.updateState(data).subscribe( result => {
      if ( result.state === 'OK' ) {
        this.update(); // Will update loading
        this.utility.showSnackBar('Estado actualizado.!!');
      }
      else {
        this.loading = false;
        this.utility.showSnackBar(result.state);
      }
    });
  }

  openDialogImg(photoInfo): void {
    const data = {
      name: photoInfo.anomalyEvidenceNameFile,
      url: photoInfo.anomalyEvidenceUrl,
      type: photoInfo.anomalyEvidenceType,
      description: photoInfo.anomalyEvidenceDescription,
    };
    const dialogRef = this.dialog.open(ModalImgComponent, {
      width: '360px',
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {});
  }

  generateReport(){
    const url = Constants.HOST_URL + 'rest/pdf/form/' + this.idReportCreated;
    window.open(url, '_blank');
  }

  private update(){
    this.api.getReportById(this.idReportCreated).subscribe( report => {
      this.report = report || {};
      this.validReport = this.isValidReport(this.report);
      this.loading = false;
    });
    this.api.getReportHistoyById(this.idReportCreated).subscribe( histories => {
      this.histories = histories;
    });

  }

  private isValidReport( report ){
    return !!report && !!report.anomalyFormState;
  }

}
