import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerBiStatsComponent } from './power-bi-stats.component';

describe('PowerBiStatsComponent', () => {
  let component: PowerBiStatsComponent;
  let fixture: ComponentFixture<PowerBiStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerBiStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerBiStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
