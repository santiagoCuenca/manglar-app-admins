import { Component, OnInit } from '@angular/core';
import { ModalAllowedUserComponent } from '../modal-allowed-user/modal-allowed-user.component';
import { ModalNotificationComponent } from '../modal-notification/modal-notification.component';
import { ModalOrganizationComponent } from '../modal-organization/modal-organization.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.sass']
})
export class ConfigComponent implements OnInit {

  organizations: any[] = [];
  emailNotifications: any[] = [];

  displayedColumns;
  displayedColumnsAllowedUsers;
  displayedColumnsOrganizations;

  helpNotifications = 'Las notificaciones que se generen a través de la aplicación móvil por socios de las organizaciones con AUSCEM serán enviados al correo de funcionarios del MAE- SGMC, MPCEIP y Fiscalía dependiendo de las competencias en la materia correspondiente.';
  helpOrganization = 'Constan las organizaciones con AUSCEM y las instituciones públicas involucradas en ManglarApp.';

  CONFIG_NOTIFICATIONS = [
    {
      label: 'Id',
      idField: 'emailNotificationId',
      type: 'inputNumber',
      disabled: true,
    },
    {
      label: 'Nombre',
      idField: 'emailNotificationName',
      type: 'inputText',
    },
    {
      label: 'Correo',
      idField: 'emailNotificationEmail',
      type: 'inputText',
    },
    {
      label: 'Provincias',
      idField: 'emailNotificationProvinces',
      type: 'inputSelect',
    },
    {
      label: 'Tipos',
      idField: 'emailNotificationAnomaliesTypes',
      type: 'inputSelect',
    },
  ];

  CONFIG_ORGANIZACIONES = [
    {
      label: 'Id',
      idField: 'organizationManglarId',
      type: 'inputNumber',
      disabled: true,
    },
    {
      label: 'Nombre',
      idField: 'organizationManglarName',
      type: 'inputText',
    },
    {
      label: 'Nombre completo',
      idField: 'organizationManglarCompleteName',
      type: 'inputText',
    },
  ];

  CONFIG_ALLOWED_USERS = [
    {
      label: 'Id',
      idField: 'allowedUserId',
      type: 'inputNumber',
      disabled: true,
    },
    {
      label: 'Nombre',
      idField: 'allowedUserName',
      type: 'inputText',
    },
    {
      label: 'Cédula',
      idField: 'allowedUserPin',
      type: 'inputText',
    },
    {
      label: 'Organización',
      idField: 'organizationManglarName',
      type: 'inputSelect',
    },
    {
      label: 'Provincia',
      idField: 'geographicalLocationName',
      type: 'inputSelect',
    },
  ];

  CONFIG_ROLES = [
    {
      id: 'super-admin',
      title: 'ADMINISTRADORES',
      help: 'Este rol les permitirá a funcionarios seleccionados del MAE-SGMC ingresar datos y editar información de las personas autorizadas de las organizaciones con AUSCEM para realizar las denuncias por ManglarApp y mantener actualizada la lista de funcionarios del MAE-SGMC, MPCEIP y Fiscalía para el ingreso al sistema.',
      values: [],
    },
    {
      id: 'admin',
      title: 'TÉCNICOS',
      help: 'Listado de funcionarios del MAE-SGMC, MPCEIP y Fiscalía, autorizados paraingresar a la plataforma web de ManglarApp, revisar reportes y generarreportes estadísticos.',
      values: [],
    },
    {
      id: 'socio',
      title: 'SOCIOS',
      help: 'Listado de socios de organizaciones con AUSCEM autorizados para ingresar a la aplicación móvil de ManglarApp y realizar denuncias o notificaciones de anomalías en el manglar.',
      values: [],
    },
  ];

  constructor(
    private api: Api,
    private utility: Utility,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.loadValues();
    this.displayedColumns = this.CONFIG_NOTIFICATIONS.map( config => config.idField );
    this.displayedColumns.push('status');
    this.displayedColumns.push('options');
    this.displayedColumnsOrganizations = this.CONFIG_ORGANIZACIONES.map( config => config.idField );
    this.displayedColumnsOrganizations.push('status');
    this.displayedColumnsOrganizations.push('options');
    this.displayedColumnsAllowedUsers = this.CONFIG_ALLOWED_USERS.map( config => config.idField );
    this.displayedColumnsAllowedUsers.push('status');
    this.displayedColumnsAllowedUsers.push('options');
  }

  private loadValues(){
    this.api.getOrganizations().subscribe( organizations => {
      this.organizations = organizations
      .filter( o => o.organizationManglarType === 'socio' )
      .sort( (a, b) => a.organizationManglarId - b.organizationManglarId );
    });
    this.api.getEmailNotifications().subscribe( emailNotifications => {
      this.emailNotifications = emailNotifications.sort( (a, b) => a.emailNotificationId - b.emailNotificationId );
    });
    this.CONFIG_ROLES.forEach( configRole => {
      this.api.getAllowedUsers(configRole.id).subscribe( allowedUsers => {
        configRole.values = allowedUsers.sort( (a, b) => a.allowedUserId - b.allowedUserId );
      });
    });
  }

  editAllowedUser(data, type): void {
    this.openDialogAllowedUser(data, type);
  }

  private openDialogAllowedUser(data, type){
    const dialogRef = this.dialog.open(ModalAllowedUserComponent, {
      data: Object.assign({type}, data),
      width: '360px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.api.saveAllowedUser(result, type).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se guardó correctamente');
        } else {
          this.utility.showSnackBar(result.state);
        }
        this.loadValues();
      });
    });
  }

  createNewAllowedUser(type){
    const data = {
      allowedUserName: '',
      allowedUserPin: '',
      allowedUserStatus: true,
      geographicalLocationName: '',
      organizationManglarName: '',
    };
    this.openDialogAllowedUser(data, type);
  }

  removeAllowedUser(element, type){
    this.utility.confirmDialog('Atención', 'Esta seguro de eliminar', () => {
      const data = {id: element.allowedUserId};
      this.api.removeAllowedUser(data).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se eliminó correctamente');
        }
        this.loadValues();
      });
    });
  }

  editNotification(data){
    this.openDialogNotification(data);
  }

  private openDialogNotification(data){
    const dialogRef = this.dialog.open(ModalNotificationComponent, {
      data: Object.assign({}, data),
      width: '360px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.api.saveEmailNotification(result).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se guardó correctamente');
        } else {
          this.utility.showSnackBar(result.state);
        }
        this.loadValues();
      });
    });
  }

  createNewNotification(){
    const data = {
      emailNotificationAnomaliesTypes: '[]',
      emailNotificationEmail: '',
      emailNotificationName: '',
      emailNotificationProvinces: '[]',
      emailNotificationStatus: true,
    };
    this.openDialogNotification(data);
  }

  getValue(element, id){
    const value = element[id];
    if (id !== 'emailNotificationProvinces' && id !== 'emailNotificationAnomaliesTypes'){
      return value;
    }
    if ( value === 'ALL' || value === 'TODOS' ) return 'TODOS';
    const parse = JSON.parse(value).join(', ');
    return this.utility.limitString(parse, 22).toUpperCase();
  }

  removeNotification(element){
    this.utility.confirmDialog('Atención', 'Esta seguro de eliminar', () => {
      const data = {id: element.emailNotificationId};
      this.api.removeEmailNotification(data).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se eliminó correctamente');
        }
        this.loadValues();
      });
    });
  }

  removeOrganization(element){
    this.utility.confirmDialog('Atención', 'Esta seguro de eliminar', () => {
      const data = {id: element.organizationManglarId};
      this.api.removeOrganization(data).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se eliminó correctamente');
        }
        this.loadValues();
      });
    });
  }

  editOrganization(data){
    this.openDialogOrganization(data);
  }

  private openDialogOrganization(data){
    const dialogRef = this.dialog.open(ModalOrganizationComponent, {
      data: Object.assign({}, data),
      width: '360px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) return;
      this.api.saveOrganization(result).subscribe( result => {
        if ( result.state === 'OK' ) {
          this.utility.showSnackBar('Se guardó correctamente');
        } else {
          this.utility.showSnackBar(result.state);
        }
        this.loadValues();
      });
    });
  }

  createNewOrganization(){
    const data = {
      organizationManglarCompleteName: '',
      organizationManglarName: '',
      organizationManglarStatus: true,
      organizationManglarType: 'socio',
    };
    this.openDialogOrganization(data);
  }

  changeStatusNotification($event, notification){
    notification.emailNotificationStatus = $event.checked;
    this.api.saveEmailNotification(notification).subscribe( result => {
      if ( result.state === 'OK' ) {
        const message = $event.checked ? ' ACTIVADO' : ' DESACTIVADO';
        this.utility.showSnackBar(notification.emailNotificationName + message);
      } else {
        this.utility.showSnackBar(result.state);
      }
      this.loadValues();
    });
  }

  changeStatusOrganization($event, organization){
    organization.organizationManglarStatus = $event.checked;
    this.api.saveOrganization(organization).subscribe( result => {
      if ( result.state === 'OK' ) {
        const message = $event.checked ? ' ACTIVADO' : ' DESACTIVADO';
        this.utility.showSnackBar(organization.organizationManglarName + message);
      } else {
        this.utility.showSnackBar(result.state);
      }
      this.loadValues();
    });
  }

  changeStatusAllowedUser($event, element, type){
    const allowedUserStatus = $event.checked;
    const dataSubmit = {
      allowedUser: {
        allowedUserStatus,
        allowedUserId: element.allowedUserId,
        allowedUserName: element.allowedUserName,
        allowedUserPin: element.allowedUserPin,
      },
      geloId: element.geloId,
      organizationManglarId: element.organizationManglarId,
    };
    this.api.saveAllowedUser(dataSubmit, type).subscribe( result => {
      if ( result.state === 'OK' ) {
        const message = allowedUserStatus ? ' ACTIVADO' : ' DESACTIVADO';
        this.utility.showSnackBar(element.allowedUserName + message);
      } else {
        this.utility.showSnackBar(result.state);
      }
      this.loadValues();
    });
  }

}
