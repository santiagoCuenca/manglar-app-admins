import { Component, OnInit, Input } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Axis from 'd3-axis';
import * as d3Array from 'd3-array';

export interface Margin {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

@Component({
  selector: 'app-bars',
  templateUrl: './bars.component.html',
  styleUrls: ['./bars.component.sass']
})
export class BarsComponent implements OnInit {

  @Input() configGroup: any;
  @Input() widthChart: number;
  @Input() heightChart: number;

  data = [];

  private margin: Margin;

  private width: number;
  private height: number;

  private svg: any;     // TODO replace all `any` by the right type

  private x: any;
  private y: any;
  private z: any;
  private g: any;

  constructor() {}

  ngOnInit() {
    this.updateChart(this.configGroup.bars);
  }

  ngDoCheck() {
    if ( JSON.stringify(this.configGroup.bars) !== JSON.stringify(this.data)){
      this.updateChart(this.configGroup.bars);
    }
  }

  private updateChart(data){
    this.data = data;
    d3.select('.bar-wrapper svg').select('g').remove();
    d3.select('.bar-wrapper svg').remove();
    this.initSvg();
    this.drawChart(this.data);
  }

  private initSvg() {
    this.margin = {top: 20, right: 95, bottom: 30, left: 30};
    this.width = this.widthChart - this.margin.left - this.margin.right;
    this.height = this.heightChart - this.margin.top - this.margin.bottom;
    d3.select('.bar-wrapper').append('svg').attr('width', this.widthChart).attr('height', this.heightChart);
    this.svg = d3.select('.bar-wrapper svg');

    this.g = this.svg.append('g').attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    this.x = d3Scale.scaleBand()
      .rangeRound([0, this.width])
      .paddingInner(0.05)
      .align(0.1);
    this.y = d3Scale.scaleLinear()
      .rangeRound([this.height, 0]);
    this.z = d3Scale.scaleOrdinal()
      .range(['#5DC98F', '#3FBE70', '#35A053', '#26714D', '#38A9A2', '#5AC8BA', '#83D277', '#89C146']);
  }

  private getkeys(data){
    let keysSet = new Set();
    data.forEach(d => {
      Object.getOwnPropertyNames(d).slice(1).forEach(i => keysSet.add(i));
    });
    return Array.from(keysSet);
  }

  private drawChart(data: any[]) {

    let keys = this.getkeys(data);

    data = data.map(v => {
      keys.forEach(key => {
        if (!v[key]) v[key] = 0;
      });
      v.total = keys.map(key => v[key]).reduce((a, b) => a + b, 0);
      return v;
    });

    data.sort((a: any, b: any) => b.total - a.total);

    this.x.domain(data.map((d: any) => d.Province));
    this.y.domain([0, d3Array.max(data, (d: any) => d.total)]).nice();
    this.z.domain(keys);

    this.g.append('g')
      .selectAll('g')
      .data(d3Shape.stack().keys(keys)(data))
      .enter().append('g')
      .attr('fill', d => {
        d.forEach( dI => { dI['key'] = d.key}); // Include key to show on tooltip
        return this.z(d.key);
      })
      .selectAll('rect')
      .data(d => d)
      .enter().append('rect')
      .attr('x', d => this.x(d.data.Province))
      .attr('y', d => this.y(d[1]))
      .attr('height', d => this.y(d[0]) - this.y(d[1]))
      .attr('width', this.x.bandwidth())
      .on('mouseover', function() { tooltip.style('display', null); })
      .on('mouseout', function() { tooltip.style('display', 'none'); })
      .on('mousemove', function(d) {
        var xPosition = d3.mouse(this)[0] - 5;
        var yPosition = d3.mouse(this)[1] - 5;
        tooltip.attr('transform', 'translate(' + xPosition + ',' + yPosition + ')');
        tooltip.select('text').text(d.key + ' ' + (d[1]-d[0]) );
      });

    this.g.append('g')
      .attr('class', 'axis')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x));

    this.g.append('g')
      .attr('class', 'axis')
      .call(d3Axis.axisLeft(this.y).ticks(null, 's'))
      .append('text')
      .attr('x', this.width/2)
      .attr('y', this.y(this.y.ticks().pop()) - 10)
      .attr('dy', '0.32em')
      .attr('fill', '#000')
      .attr('font-weight', 'bold')
      .attr('text-anchor', 'start')
      .text(this.configGroup.filters[0].label);

    // Prep the tooltip bits, initial display is hidden
    let tooltip = this.svg.append('g')
      .attr('class', 'tooltip')
      .style('display', 'none');

    tooltip.append('rect')
      .attr('width', 90)
      .attr('height', 20)
      .attr('fill', 'white')
      .style('opacity', 0.8);

    tooltip.append('text')
      .attr('x', 45)
      .attr('dy', '1.2em')
      .style('text-anchor', 'middle')
      .attr('font-size', '10px')
      .attr('font-weight', 'bold');

    let legend = this.g.append('g')
      .attr('font-family', 'sans-serif')
      .attr('font-size', 9)
      .attr('text-anchor', 'end')
      .selectAll('g')
      .data(keys.slice().reverse())
      .enter().append('g')
      .attr('transform', (d, i) => 'translate(' + (this.margin.right-10) + ',' + i * 19 + ')');

    legend.append('rect')
      .attr('x', this.width - 22)
      .attr('width', 18)
      .attr('height', 18)
      .attr('fill', this.z);

    legend.append('text')
      .attr('x', this.width - 24)
      .attr('y', 9.5)
      .attr('dy', '0.32em')
      .text(d => d);
  }

}
