import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaeInfoComponent } from './mae-info.component';

describe('MaeInfoComponent', () => {
  let component: MaeInfoComponent;
  let fixture: ComponentFixture<MaeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
