import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { PasswordComponent } from './password/password.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { NormativesComponent } from './normatives/normatives.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'reset-password', component: PasswordComponent },
  { path: 'reset-password/:param1', component: PasswordResetComponent },
  { path: 'normatives', component: NormativesComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'dashboard', redirectTo: '/dashboard/information', pathMatch: 'full' },
  { path: 'dashboard/:param1', component: DashboardComponent },
  { path: 'dashboard/:param1/:param2', component: DashboardComponent },
  { path: 'dashboard/:param1/:param2/:param3', component: DashboardComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
