import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

export interface DialogData {
  organizationManglarId: number;
  organizationManglarCompleteName: string;
  organizationManglarName: string;
  organizationManglarStatus: boolean;
  organizationManglarType: string;
}
@Component({
  selector: 'app-modal-organization',
  templateUrl: './modal-organization.component.html',
  styleUrls: ['./modal-organization.component.sass']
})
export class ModalOrganizationComponent {

  constructor(
    public dialogRef: MatDialogRef<ModalOrganizationComponent>,
    private api: Api,
    private utility: Utility,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(){
    if (this.data.organizationManglarName === '' || this.data.organizationManglarCompleteName === '' ){
      this.utility.showSnackBar('ERROR: Debe incluir todos los campos');
      return;
    }
    this.dialogRef.close(this.data);
  }

}
