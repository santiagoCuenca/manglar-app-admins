import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import 'moment/locale/es';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MatIconModule } from '@angular/material/icon';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { UserIdleModule } from 'angular-user-idle';
import { BotDetectCaptchaModule } from 'angular-captcha';

// Services
import { ApiCall } from './services/api-call';
import { Api } from './services/api';
import { Utility } from './util/utility';

// pipes
import { FilterReportPipe } from './filters/filterReport';

import { environments } from '../environments/environments';
import { TabsComponent } from './tabs/tabs.component';
import { InformationComponent } from './information/information.component';
import { ReportsComponent } from './reports/reports.component';
import { MomentModule } from 'angular2-moment';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';

// ANGULAR MATERIAL
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatTreeModule } from '@angular/material/tree';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { ReportsTabComponent } from './reports-tab/reports-tab.component';
import { ModalSelectComponent } from './modal-select/modal-select.component';
import { BarsComponent } from './bars/bars.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { ModalImgComponent } from './modal-img/modal-img.component';
import { AppWrapperComponent } from './app-wrapper/app-wrapper.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { SystemInfoComponent } from './system-info/system-info.component';
import { MaeInfoComponent } from './mae-info/mae-info.component';
import { RegisterComponent } from './register/register.component';
import { BlockInfoComponent } from './block-info/block-info.component';
import { ConfigComponent } from './config/config.component';
import { ModalAllowedUserComponent } from './modal-allowed-user/modal-allowed-user.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ModalNotificationComponent } from './modal-notification/modal-notification.component';
import { ModalOrganizationComponent } from './modal-organization/modal-organization.component';
import { ProfileComponent } from './profile/profile.component';
import { PowerBiStatsComponent } from './power-bi-stats/power-bi-stats.component';
import { NormativesComponent } from './normatives/normatives.component';
import { PasswordComponent } from './password/password.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ModalHistoryChangesComponent } from './modal-history-changes/modal-history-changes.component';
import { PowerBiExtraInfoComponent } from './power-bi-extra-info/power-bi-extra-info.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PageNotFoundComponent,
    TabsComponent,
    InformationComponent,
    ReportsComponent,
    ReportDetailComponent,
    ReportsTabComponent,
    ModalSelectComponent,
    BarsComponent,
    SnackBarComponent,
    ModalImgComponent,
    AppWrapperComponent,
    UserInfoComponent,
    SystemInfoComponent,
    MaeInfoComponent,
    RegisterComponent,
    BlockInfoComponent,
    ConfigComponent,
    ModalAllowedUserComponent,
    ConfirmDialogComponent,
    FilterReportPipe,
    ModalNotificationComponent,
    ModalOrganizationComponent,
    ProfileComponent,
    PowerBiStatsComponent,
    NormativesComponent,
    PasswordComponent,
    PasswordResetComponent,
    ModalHistoryChangesComponent,
    PowerBiExtraInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot(),
    AppRoutingModule,
    MomentModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatDialogModule,
    MatGridListModule,
    MatRadioModule,
    MatSnackBarModule,
    CdkTreeModule,
    MatTreeModule,
    FormsModule,
    BotDetectCaptchaModule,
    MatIconModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatVideoModule,
    // Optionally you can set time for `idle`, `timeout` and `ping` in seconds.
    // Default values: `idle` is 600 (10 minutes), `timeout` is 300 (5 minutes)
    // and `ping` is 120 (2 minutes).
    UserIdleModule.forRoot({idle: 600, timeout: 10, ping: 120})
  ],
  entryComponents: [
    ModalSelectComponent,
    ModalHistoryChangesComponent,
    SnackBarComponent,
    ModalImgComponent,
    ModalAllowedUserComponent,
    ConfirmDialogComponent,
    ModalNotificationComponent,
    ModalOrganizationComponent
  ],
  providers: [
    ApiCall,
    Api,
    Utility
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
