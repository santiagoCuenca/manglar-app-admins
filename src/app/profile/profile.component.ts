import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {

  currentPassword: string = '';
  newPassword: string = '';
  confirmPassword: string = '';

  nationalities: any[] = [];

  provinces: any[] = [];
  cantons: any[] = [];
  parroquias: any[] = [];
  entities: any[] = ['Persona Natural'];
  documents: any[] = ['Cédula'];

  isLoading: boolean = true;
  user: any = {};

  selectedEntity = 'Persona Natural';
  selectedDocument = 'Cédula';
  selectedProvince: any;
  selectedCanton: any;
  selectedParroquia: any;
  selectedNationality: any;
  username: string = '';

  constructor(
    private api: Api,
    private utility: Utility,
    private router: Router,
  ) { }

  ngOnInit() {
    if ( this.utility.isNotLogged() ) {
      this.logout();
      setTimeout( () => this.utility.showSnackBar('Usuario no inició sesión'));
      return;
    }
    // PIN
    const pin = this.utility.getLocalStorage('userpin');

    this.api.getUserProfile(pin).subscribe( user => {
      this.user = user;
      console.log('this.user', this.user);
      this.selectLocations(this.user, () => {
        this.isLoading = false;
      });
    });

    this.username = this.utility.getLocalStorage('username');
  }

  selectLocations(user, success){
    this.api.getNationalities().subscribe( nationalities => {
      this.nationalities = nationalities;
      this.selectedNationality = nationalities.find( n => n.id === user.nationalityId);
      this.api.getLocations(1).subscribe( provinces => {
        this.provinces = provinces;
        this.selectedProvince = provinces.find( p => p.id === user.provinceId);
        this.changeCantons(this.selectedProvince, user.cantonId, () => {
          this.changeParroquias(this.selectedCanton, user.parroquiaId, () => {
            success();
          });
        });
      });
    });
  }

  changePassword(){
    if (this.currentPassword === '' || this.newPassword === '' || this.confirmPassword === ''){
      this.utility.showSnackBar('Debe llenar todos los campos');
      return;
    }
    if (this.newPassword !== this.confirmPassword){
      this.utility.showSnackBar('La nueva contraseña y la confirmación no coinciden');
      return;
    }
    this.utility.showLoading();
    const data = {
      userPin: this.utility.getLocalStorage('userpin'),
      password: this.currentPassword,
      newPassword: this.newPassword,
      newPasswordConfirm: this.confirmPassword,
    };
    this.api.changePassword(data).subscribe( result => {
      this.utility.hideLoading();
      if (result.state === 'OK') {
        this.utility.showSnackBar('Se cambió con exito, debe iniciar con su nueva clave');
        this.logout();
        return;
      }
      this.utility.showSnackBar(result.state);
    });
  }

  logout(){
    this.utility.logout();
    this.router.navigate(['/login']);
  }

  changeCantons(selectedProvince, cantonSelected, success?){
    this.api.getLocations(selectedProvince.id).subscribe( cantons => {
      this.cantons = cantons;
      this.selectedCanton = this.cantons.find( c => c.id === cantonSelected);
      if (success) success();
    });
  }

  changeParroquias(selectedCanton, parroquiaSelected, success?){
    this.api.getLocations(selectedCanton.id).subscribe( parroquias => {
      this.parroquias = parroquias;
      this.selectedParroquia = this.parroquias.find( p => p.id === parroquiaSelected);
      if (success) success();
    });
  }

  saveProfile(){
    if ( !this.selectedCanton || !this.selectedParroquia ) {
      this.utility.showSnackBar('Debe seleccionar su ubicación');
      return;
    }

    const location = {
      provinceId: this.selectedProvince.id,
      cantonId: this.selectedCanton.id,
      parroquiaId: this.selectedParroquia.id,
      nationalityId: this.selectedNationality.id,
    };
    const data = Object.assign({}, this.user, location);

    if ( !data.address || data.address === '') {
      this.utility.showSnackBar('Debe ingresar una dirección');
      return;
    }

    if ( !data.email || data.email === '') {
      this.utility.showSnackBar('Debe ingresar una correo');
      return;
    }
    if ( !data.mobile || data.mobile === '') {
      this.utility.showSnackBar('Debe ingresar un número de celular');
      return;
    }
    if ( !data.phone || data.phone === '') {
      this.utility.showSnackBar('Debe ingresar un teléfono');
      return;
    }

    this.api.saveProfile(data).subscribe( result => {
      if ( result.state === 'OK' ) {
        this.utility.showSnackBar('Se guardó correctamente');
        this.router.navigate(['/dashboard']);
      } else {
        this.utility.showSnackBar(result.state);
      }
    });
  }

  cancel(){
    this.router.navigate(['/dashboard']);
  }

}
