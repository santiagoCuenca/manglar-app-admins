import { Component, OnInit } from '@angular/core';
import { Api } from '../services/api';
import { Utility } from '../util/utility';

@Component({
  selector: 'app-reports-tab',
  templateUrl: './reports-tab.component.html',
  styleUrls: ['./reports-tab.component.sass']
})
export class ReportsTabComponent implements OnInit {

  reportsInfo: any[] = [];
  isLoading: boolean = true;

  constructor(
    private api: Api,
    private utility: Utility,
  ) { }

  ngOnInit() {
    this.api.getReportsSummary().subscribe( reportsApi => {
      const reports = reportsApi || [];
      this.reportsInfo = this.utility.getFormatReportsSummary(reports);
      this.isLoading = false;
    });
  }

}
